---
release_number: "17.8" # version number - required
title: "GitLab 17.8 released with improved container repository security" # short title (no longer than 62 characters) - required
author: Joe Randazzo # author name and surname - required
author_gitlab: jrandazzo # author's gitlab.com username - required
image_title: '/images/17_8/product-gl17-release-cover-17-8-0093-1440x400-fy25.png' # cover image - required
description: "GitLab 17.8 released with enhanced security for container repositories, ML model experiments tracking, list deployments related to a release, Hosted runners on Linux for GitLab Dedicated, and much more!" # short description - required
twitter_image: '/images/17_8/product-gl17-release-cover-17-8-0093-1440x400-fy25.png' # required - copy URL from image title section above
categories: releases # required
layout: release # required
featured: yes
rebrand_cover_img: true

# APPEARANCE
# header_layout_dark: true #uncomment if the cover image is dark
# release_number_dark: true #uncomment if you want a dark release number
# release_number_image: "/images/X_Y/X_Y-release-number-image.svg" # uncomment if you want a svg image to replace the release number that normally overlays the background image

---

<!--
This is the release blog post file. Add here the introduction only.
All remaining content goes into data/release-posts/.

**Use the merge request template "Release-Post", and please set the calendar due
date for each stage (general contributions, review).**

Read through the Release Posts Handbook for more information:
https://about.gitlab.com/handbook/marketing/blog/release-posts/#introduction
-->

Today, we are excited to announce the release of GitLab 17.8 including [enhanced security for container repositories](#enhance-security-with-protected-container-repositories), [list deployments related to a release](#list-the-deployments-related-to-a-release), [ML model experiments tracking](#machine-learning-model-experiments-tracking-in-ga), [Hosted runners on Linux for GitLab Dedicated](#hosted-runners-on-linux-for-gitlab-dedicated-now-in-limited-availability), and much more!

These are just a few highlights from the 60+ improvements in this release. Read on to check out all of the great updates below.

To the wider GitLab community, thank you for the 121 contributions you provided to GitLab 17.8!
At GitLab, [everyone can contribute](https://about.gitlab.com/community/contribute/) and we couldn't have done it without you!

To preview what's coming in next month’s release, check out our [Upcoming Releases page](/direction/kickoff/), which includes our 17.9 release kickoff video.
