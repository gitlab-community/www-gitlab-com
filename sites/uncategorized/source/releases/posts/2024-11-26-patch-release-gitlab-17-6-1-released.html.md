---
title: "GitLab Patch Release: 17.6.1, 17.5.3, 17.4.5"
categories: releases
author: Ottilia Westerlund
author_gitlab: ottilia_westerlund
author_twitter: gitlab
description: "Learn more about GitLab Patch Release: 17.6.1, 17.5.3, 17.4.5 for GitLab Community Edition (CE) and Enterprise Edition (EE)."
canonical_path: '/releases/2024/11/26/patch-release-gitlab-17-6-1-released/'
image_title: '/images/blogimages/security-cover-new.png'
tags: security
---

<!-- For detailed instructions on how to complete this, please see https://gitlab.com/gitlab-org/release/docs/-/blob/master/general/patch/blog-post.md -->

Today we are releasing versions 17.6.1, 17.5.3, 17.4.5 for GitLab Community Edition (CE) and Enterprise Edition (EE).

These versions contain important bug and security fixes, and we strongly recommend that all self-managed GitLab installations be upgraded to
one of these versions immediately. GitLab.com is already running the patched version. GitLab Dedicated customers do not need to take action.

GitLab releases fixes for vulnerabilities in patch releases. There are two types of patch releases:
scheduled releases, and ad-hoc critical patches for high-severity vulnerabilities. Scheduled releases are released twice a month on the second and fourth Wednesdays.
For more information, you can visit our [releases handbook](https://handbook.gitlab.com/handbook/engineering/releases/) and [security FAQ](https://about.gitlab.com/security/faq/).
You can see all of GitLab release blog posts [here](/releases/categories/releases/).

For security fixes, the issues detailing each vulnerability are made public on our
[issue tracker](https://gitlab.com/gitlab-org/gitlab/-/issues/?sort=created_date&state=closed&label_name%5B%5D=bug%3A%3Avulnerability&confidential=no&first_page_size=100)
30 days after the release in which they were patched.

We are committed to ensuring all aspects of GitLab that are exposed to customers or that host customer data are held to
the highest security standards. As part of maintaining good security hygiene, it is highly recommended that all customers
upgrade to the latest patch release for their supported version. You can read more
[best practices in securing your GitLab instance](/blog/2020/05/20/gitlab-instance-security-best-practices/) in our blog post.

### Recommended Action

We **strongly recommend** that all installations running a version affected by the issues described below are **upgraded to the latest version as soon as possible**.

When no specific deployment type (omnibus, source code, helm chart, etc.) of a product is mentioned, this means all types are affected.

## Security fixes

### Table of security fixes

| Title | Severity |
| ----- | -------- |
| [Privilege Escalation via LFS Tokens](#privilege-escalation-via-lfs-tokens) | High |
| [DoS through uncontrolled resource consumption when viewing a maliciously crafted cargo.toml file.](#dos-through-uncontrolled-resource-consumption-when-viewing-a-maliciously-crafted-cargotoml-file) | Medium |
| [Unintended access to Usage Data via Scoped Tokens](#unintended-access-to-usage-data-via-scoped-tokens) | Medium |
| [Gitlab DOS via Harbor registry integration](#gitlab-dos-via-harbor-registry-integration) | Medium |
| [Resource exhaustion and denial of service with test_report API calls](#resource-exhaustion-and-denial-of-service-with-test_report-api-calls) | Medium |
| [Streaming endpoint did not invalidate tokens after revocation](#streaming-endpoint-did-not-invalidate-tokens-after-revocation) | Medium |

### Privilege Escalation via LFS Tokens

An issue has been discovered in GitLab CE/EE affecting all versions from 8.12 before 17.4.5, 17.5 before 17.5.3, and 17.6 before 17.6.1. This issue allows an attacker with access to a victim's Personal Access Token (PAT) to escalate privileges.
This is a high severity issue ([`CVSS:3.1/AV:N/AC:H/PR:L/UI:N/S:C/C:H/I:H/A:N `](https://gitlab-com.gitlab.io/gl-security/product-security/appsec/cvss-calculator/explain#explain=CVSS:3.1/AV:N/AC:H/PR:L/UI:N/S:C/C:H/I:H/A:N ), 8.2).
It is now mitigated in the latest release and is assigned [CVE-2024-8114](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2024-8114).

Thanks [pwnie](https://hackerone.com/pwnie) for reporting this vulnerability through our HackerOne bug bounty program.


### DoS through uncontrolled resource consumption when viewing a maliciously crafted cargo.toml file.

A Denial of Service (DoS) issue has been discovered in GitLab CE/EE affecting all versions prior to 12.6 prior to 17.4.5, 17.5 prior to 17.5.3, and 17.6 prior to 17.6.1. An attacker could cause a denial of service with a crafted cargo.toml file.
This is a medium severity issue ([`CVSS:3.1/AV:N/AC:L/PR:L/UI:N/S:U/C:N/I:N/A:H`](https://gitlab-com.gitlab.io/gl-security/product-security/appsec/cvss-calculator/explain#explain=CVSS:3.1/AV:N/AC:L/PR:L/UI:N/S:U/C:N/I:N/A:H), 6.5).
It is now mitigated in the latest release and is assigned [CVE-2024-8237](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2024-8237).

Thanks [l33thaxor](https://hackerone.com/l33thaxor) for reporting this vulnerability through our HackerOne bug bounty program.


### Unintended Access to Usage Data via Scoped Tokens

An issue was discovered in GitLab CE/EE affecting all versions from 16.9.8 before 17.4.5, 17.5 before 17.5.3, and 17.6 before 17.6.1. Certain API endpoints could potentially allow unauthorized access to sensitive data due to overly broad application of token scopes.
This is a medium severity issue ([`CVSS:3.1/AV:N/AC:L/PR:H/UI:N/S:U/C:H/I:H/A:N`](https://gitlab-com.gitlab.io/gl-security/product-security/appsec/cvss-calculator/explain#explain=CVSS:3.1/AV:N/AC:L/PR:H/UI:N/S:U/C:H/I:H/A:N), 6.5).
It is now mitigated in the latest release and is assigned [CVE-2024-11669](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2024-11669).

This vulnerability has been discovered internally by a GitLab team member, [Dylan Griffith](https://gitlab.com/DylanGriffith).


### Gitlab DOS via Harbor registry integration

An issue was discovered in GitLab CE/EE affecting all versions starting from 15.6 prior to 17.4.5, starting from 17.5 prior to 17.5.3, starting from 17.6 prior to 17.6.1 which could cause Denial of Service via integrating a malicious harbor registry.
This is a medium severity issue ([`CVSS:3.1/AV:N/AC:H/PR:L/UI:N/S:U/C:N/I:N/A:H`](https://gitlab-com.gitlab.io/gl-security/product-security/appsec/cvss-calculator/explain#explain=CVSS:3.1/AV:N/AC:H/PR:L/UI:N/S:U/C:N/I:N/A:H), 5.3).
It is now mitigated in the latest release and is assigned [CVE-2024-8177](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2024-8177).

Thanks [a92847865](https://hackerone.com/a92847865) for reporting this vulnerability through our HackerOne bug bounty program.


### Resource exhaustion and denial of service with test_report API calls

A denial of service (DoS) condition was discovered in GitLab CE/EE affecting all versions from 13.2.4 before 17.4.5, 17.5 before 17.5.3, and 17.6 before 17.6.1. By leveraging this vulnerability an attacker could create a DoS condition by sending crafted API calls.
This is a medium severity issue ([`CVSS:3.1/AV:N/AC:L/PR:L/UI:N/S:U/C:N/I:N/A:L`](https://gitlab-com.gitlab.io/gl-security/product-security/appsec/cvss-calculator/explain#explain=CVSS:3.1/AV:N/AC:L/PR:L/UI:N/S:U/C:N/I:N/A:L), 4.3).
It is now mitigated in the latest release and is assigned [CVE-2024-11828](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2024-11828).

Thanks [luryus](https://hackerone.com/luryus) for reporting this vulnerability through our HackerOne bug bounty program.


### Streaming endpoint did not invalidate tokens after revocation

An issue has been discovered in GitLab CE/EE affecting all versions from 16.11 before 17.4.5, 17.5 before 17.5.3, and 17.6 before 17.6.1. Long-lived connections could potentially bypass authentication controls, allowing unauthorized access to streaming results.
This is a medium severity issue ([`CVSS:3.1/AV:N/AC:H/PR:L/UI:N/S:U/C:L/I:L/A:N`](https://gitlab-com.gitlab.io/gl-security/product-security/appsec/cvss-calculator/explain#explain=CVSS:3.1/AV:N/AC:H/PR:L/UI:N/S:U/C:L/I:L/A:N), 4.2).
It is now mitigated in the latest release and is assigned [CVE-2024-11668](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2024-11668).

This vulnerability has been discovered internally by GitLab team members, [Dylan Griffith](https://gitlab.com/DylanGriffith) and [Heinrich Lee Yu](https://gitlab.com/engwan).


## Bug fixes


### 17.6.1

* [Revert "Merge branch 'include-sec-in-sidekiq-worker-attributes-concern' into 'master'"](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/173427)
* [Revert "Merge branch '421376-part-1-move-history-button' into 'master'"](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/173288)
* [Backport to 17.6 the fix for sbom ingestion failure when license spdx id is nil](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/173464)
* [Cherry-pick 'jennli-patch-compile-prod-assets-rules' into 17-6-stable-ee](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/173593)

### 17.5.3

* [Disable http router in tests](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/172339)
* [Ensure auto_merge_enabled is set when validating merge trains](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/171715)
* [Backport Index work items when project visibility level changes](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/172391)
* [Backport fix for token revocation to 17.5](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/172767)
* [Backport Publish AuthorizationsAddedEvent with multiple projects](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/172871)
* [Make assertion order independent](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/173185)
* [Cherry-pick 'jennli-patch-compile-prod-assets-rules' into 17-5-stable-ee](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/173594)

## Updating

To update GitLab, see the [Update page](/update).
To update Gitlab Runner, see the [Updating the Runner page](https://docs.gitlab.com/runner/install/linux-repository.html#updating-the-runner).

## Receive Patch Notifications

To receive patch blog notifications delivered to your inbox, visit our [contact us](https://about.gitlab.com/company/contact/) page.
To receive release notifications via RSS, subscribe to our [patch release RSS feed](https://about.gitlab.com/security-releases.xml) or our [RSS feed for all releases](https://about.gitlab.com/all-releases.xml).

## We’re combining patch and security releases

This improvement in our release process matches the industry standard and will help GitLab users get information about security and
bug fixes sooner, [read the blog post here](https://about.gitlab.com/blog/2024/03/26/were-combining-patch-and-security-releases/).
