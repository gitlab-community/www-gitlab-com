---
title: "GitLab Patch Release: 17.9.1, 17.8.4, 17.7.6"
categories: releases
author: Costel Maxim
author_gitlab: cmaxim
author_twitter: gitlab
description: "Learn more about GitLab Patch Release: 17.9.1, 17.8.4, 17.7.6 for GitLab Community Edition (CE) and Enterprise Edition (EE)."
image_title: '/images/blogimages/security-cover-fy26.png'
tags: security
---

<!-- For detailed instructions on how to complete this, please see https://gitlab.com/gitlab-org/release/docs/-/blob/master/general/patch/blog-post.md -->

Today we are releasing versions 17.9.1, 17.8.4, 17.7.6 for GitLab Community Edition (CE) and Enterprise Edition (EE).

These versions contain important bug and security fixes, and we strongly recommend that all self-managed GitLab installations be upgraded to
one of these versions immediately. GitLab.com is already running the patched version. GitLab Dedicated customers do not need to take action.

GitLab releases fixes for vulnerabilities in patch releases. There are two types of patch releases:
scheduled releases, and ad-hoc critical patches for high-severity vulnerabilities. Scheduled releases are released twice a month on the second and fourth Wednesdays.
For more information, you can visit our [releases handbook](https://handbook.gitlab.com/handbook/engineering/releases/) and [security FAQ](https://about.gitlab.com/security/faq/).
You can see all of GitLab release blog posts [here](/releases/categories/releases/).

For security fixes, the issues detailing each vulnerability are made public on our
[issue tracker](https://gitlab.com/gitlab-org/gitlab/-/issues/?sort=created_date&state=closed&label_name%5B%5D=bug%3A%3Avulnerability&confidential=no&first_page_size=100)
30 days after the release in which they were patched.

We are committed to ensuring all aspects of GitLab that are exposed to customers or that host customer data are held to
the highest security standards. As part of maintaining good security hygiene, it is highly recommended that all customers
upgrade to the latest patch release for their supported version. You can read more
[best practices in securing your GitLab instance](/blog/2020/05/20/gitlab-instance-security-best-practices/) in our blog post.

### Recommended Action

We **strongly recommend** that all installations running a version affected by the issues described below are **upgraded to the latest version as soon as possible**.

When no specific deployment type (omnibus, source code, helm chart, etc.) of a product is mentioned, this means all types are affected.

## Security fixes

### Table of security fixes

| Title | Severity |
| ----- | -------- |
| [XSS in k8s proxy endpoint](#xss-in-k8s-proxy-endpoint) | High |
| [XSS Maven Dependency Proxy](#xss-in-maven-dependency-proxy) | High |
| [HTML injection leads to XSS on self hosted instances](#html-injection-leads-to-xss-on-self-hosted-instances) | Medium |
| [Improper Authorisation Check Allows Guest User to Read Security Policy](#improper-authorisation-check-allows-guest-user-to-read-security-policy) | Medium |
| [Planner role can read code review analytics in Private Projects](#planner-role-can-read-code-review-analytics-in-private-projects) | Medium |

### XSS in k8s proxy endpoint 

An issue has been discovered in GitLab CE/EE affecting all versions from 15.10 prior to 17.7.6, 17.8 prior to 17.8.4, and 17.9 prior to 17.9.1. A proxy feature could potentially allow unintended content rendering leading to XSS under specific circumstances.
This is a high severity issue ([`CVSS:3.1/AV:N/AC:L/PR:L/UI:R/S:C/C:H/I:H/A:N`](https://gitlab-com.gitlab.io/gl-security/product-security/appsec/cvss-calculator/explain#explain=CVSS:3.1/AV:N/AC:L/PR:L/UI:R/S:C/C:H/I:H/A:N), 8.7).
It is now mitigated in the latest release and is assigned [CVE-2025-0475](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2025-0475).

Thanks [joaxcar](https://hackerone.com/joaxcar) for reporting this vulnerability through our HackerOne bug bounty program.


### XSS Maven Dependency Proxy

A Cross Site Scripting (XSS) vulnerability in GitLab-EE affecting all versions from 16.6 prior to 17.7.6, 17.8 prior to 17.8.4, and 17.9 prior to 17.9.1 allows an attacker to bypass security controls and execute arbitrary scripts in a user's browser under specific conditions.
This is a high severity issue ([`CVSS:3.1/AV:N/AC:H/PR:L/UI:R/S:C/C:H/I:H/A:N`](https://gitlab-com.gitlab.io/gl-security/product-security/appsec/cvss-calculator/explain#explain=CVSS:3.1/AV:N/AC:H/PR:L/UI:R/S:C/C:H/I:H/A:N), 7.7).
It is now mitigated in the latest release and is assigned [CVE-2025-0555](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2025-0555).

Thanks [joaxcar](https://hackerone.com/joaxcar) for reporting this vulnerability through our HackerOne bug bounty program.


### HTML injection leads to XSS on self hosted instances

An issue has been discovered in GitLab CE/EE affecting all versions from 16.6 before 17.7.6, 17.8 before 17.8.4, and 17.9 before 17.9.1. An attacker could inject HMTL into the child item search potentially leading to XSS in certain situations.
This is a medium severity issue ([`CVSS:3.1/AV:N/AC:L/PR:L/UI:R/S:C/C:L/I:L/A:N`](https://gitlab-com.gitlab.io/gl-security/product-security/appsec/cvss-calculator/explain#explain=CVSS:3.1/AV:N/AC:L/PR:L/UI:R/S:C/C:L/I:L/A:N), 5.4).
It is now mitigated in the latest release and is assigned [CVE-2024-8186](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2024-8186).

Thanks [joaxcar](https://hackerone.com/joaxcar) for reporting this vulnerability through our HackerOne bug bounty program.


### Improper Authorisation Check Allows Guest User to Read Security Policy

A vulnerability in GitLab-EE affecting all versions from 16.2 prior to 17.7.6, 17.8 prior to 17.8.4, and 17.9 prior to 17.9.1 allows a Guest user to read Security policy YAML.
This is a medium severity issue ([`CVSS:3.1/AV:N/AC:H/PR:L/UI:N/S:U/C:H/I:N/A:N`](https://gitlab-com.gitlab.io/gl-security/product-security/appsec/cvss-calculator/explain#explain=CVSS:3.1/AV:N/AC:H/PR:L/UI:N/S:U/C:H/I:N/A:N), 5.3).
It is now mitigated in the latest release and is assigned [CVE-2024-10925](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2024-10925).

Thanks [yuki_osaki](https://hackerone.com/yuki_osaki) for reporting this vulnerability through our HackerOne bug bounty program.


### Planner role can read code review analytics in private projects

Improper authorization in GitLab EE affecting all versions from 17.7 prior to 17.7.6, 17.8 prior to 17.8.4, 17.9 prior to 17.9.1 allow users with limited permissions to access potentially sensitive project analytics data.
This is a medium severity issue ([`CVSS:3.1/AV:N/AC:L/PR:L/UI:N/S:U/C:L/I:N/A:N`](https://gitlab-com.gitlab.io/gl-security/product-security/appsec/cvss-calculator/explain#explain=CVSS:3.1/AV:N/AC:L/PR:L/UI:N/S:U/C:L/I:N/A:N), 4.3).
It is now mitigated in the latest release and is assigned [CVE-2025-0307](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2025-0307).

Thanks [weasterhacker](https://hackerone.com/weasterhacker) for reporting this vulnerability through our HackerOne bug bounty program.



## Bug fixes


### 17.9.1

* [Backport - Merge branch 'revert-e78b1a9f' into 'master'](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/182032)
* [Backport/fix ambiguous pipeline 17 9](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/181864)
* [Make it possible for ignore unexpected EOFs in SSL connections](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/182126)
* [Allow Duo Chat to be resizable on self-managed (backport)](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/182179)
* [Merge branch 'mdc/include-build-assets-image-job-sync-pipelines' into 'master'](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/182385)
* [Fix instance level dashboard by default severity override](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/182121)

### 17.8.4

* [Bump gitlab-exporter to v15.2.0](https://gitlab.com/gitlab-org/build/CNG/-/merge_requests/2233)
* [[Backport] Return false for pending_migrations? if indexing disabled](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/181136)
* [Merge branch '10443-fix-workhorse-verify' into 'master'](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/181267)
* [Revert stricter workhorse route regexes](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/181358)
* [Use primary DB when authenticating via job token in jobs API](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/181872)
* [Backport/fix ambiguous pipeline 17 8](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/181865)
* [Backport add more custom exit codes for CI/CD failures MRs and fix assets caching in scheduled cache-assets:production job](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/182098)
* [Backport fix CH version incompatibility](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/181534)
* [Merge branch 'mdc/include-build-assets-image-job-sync-pipelines' into 'master'](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/182388)
* [Make it possible for ignore unexpected EOFs in SSL connections](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/182128)
* [Update dependency gitlab-exporter to v15.2.0](https://gitlab.com/gitlab-org/omnibus-gitlab/-/merge_requests/8187)
* [[Backport] Return false for pending_migrations? if indexing disabled](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/181136)
* [Merge branch '10443-fix-workhorse-verify' into 'master'](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/181267)
* [Revert stricter workhorse route regexes](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/181358)
* [Use primary DB when authenticating via job token in jobs API](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/181872)

### 17.7.6

* [Merge branch '10443-fix-workhorse-verify' into 'master'](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/181268)
* [Revert stricter workhorse route regexes](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/181359)
* [Fix failed jobs widget polling issue](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/182151)
* [Backport fix for ambiguous created_at](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/181863)
* [Backport add more custom exit codes for CI/CD failures MRs and fix assets caching in scheduled cache-assets:production job](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/182104)
* [Merge branch 'mdc/include-build-assets-image-job-sync-pipelines' into 'master'](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/182390)
* [Make it possible for ignore unexpected EOFs in SSL connections](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/182129)
* [Merge branch '10443-fix-workhorse-verify' into 'master'](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/181268)
* [Revert stricter workhorse route regexes](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/181359)

## Updating

To update GitLab, see the [Update page](/update).
To update Gitlab Runner, see the [Updating the Runner page](https://docs.gitlab.com/runner/install/linux-repository.html#updating-the-runner).

Note: GitLab releases have skipped 17.7.5 and 17.8.3. There are no patches with these version numbers.

## Receive Patch Notifications

To receive patch blog notifications delivered to your inbox, visit our [contact us](https://about.gitlab.com/company/contact/) page.
To receive release notifications via RSS, subscribe to our [patch release RSS feed](https://about.gitlab.com/security-releases.xml) or our [RSS feed for all releases](https://about.gitlab.com/all-releases.xml).

## We’re combining patch and security releases

This improvement in our release process matches the industry standard and will help GitLab users get information about security and
bug fixes sooner, [read the blog post here](https://about.gitlab.com/blog/2024/03/26/were-combining-patch-and-security-releases/).
