features:
  primary:
  - name: "Improved workspace lifecycle with delayed termination"
    available_in: [premium, ultimate]
    gitlab_com: true
    add_ons: []
    documentation_link: 'https://docs.gitlab.com/ee/user/workspace/#automatic-workspace-stop-and-termination'
    image_url: '/images/17_6/workspace-suspend.gif'
    reporter: michelle-chen
    stage: create
    categories:
      - 'Remote Development'
    epic_url:
      - 'https://gitlab.com/groups/gitlab-org/-/epics/14910'
    description: |
      With this release, a workspace now stops rather than terminates after the configured timeout has elapsed. This feature means you can always restart your workspaces and pick up where you left off.

      By default, a workspace automatically:

      - Stops 36 hours after the workspace was last started or restarted
      - Terminates 722 hours after the workspace was last stopped

      You can configure these settings in your [GitLab agent configuration](https://docs.gitlab.com/ee/user/workspace/gitlab_agent_configuration.html).

      With this feature, a workspace remains available for approximately one month after it was stopped. This way, you get to keep your progress while optimizing workspace resources.
