---
    features:
      secondary:
      - name: "Streamlined SAST analyzer coverage for more languages"
        available_in: [core, premium, ultimate]
        documentation_link: 'https://docs.gitlab.com/ee/user/application_security/sast/#supported-languages-and-frameworks'
        reporter: connorgilbert
        stage: application_security_testing
        categories:
        - SAST
        issue_url: 'https://gitlab.com/gitlab-org/gitlab/-/issues/412060'
        description: |
          GitLab Static Application Security Testing (SAST) now scans the same [languages](https://docs.gitlab.com/ee/user/application_security/sast/#supported-languages-and-frameworks) with fewer [analyzers](https://docs.gitlab.com/ee/user/application_security/sast/analyzers/), offering a simpler, more customizable scan experience.

          In GitLab 17.0, we've replaced language-specific analyzers with [GitLab-managed rules](https://docs.gitlab.com/ee/user/application_security/sast/rules.html) in the [Semgrep-based analyzer](https://gitlab.com/gitlab-org/security-products/analyzers/semgrep) for the following languages:

          - Android
          - C and C++
          - iOS
          - Kotlin
          - Node.js
          - PHP
          - Ruby

          As [announced](https://docs.gitlab.com/ee/update/deprecations.html#sast-analyzer-coverage-changing-in-gitlab-170), we've updated the [SAST CI/CD template](https://gitlab.com/gitlab-org/gitlab/-/blob/master/lib/gitlab/ci/templates/Jobs/SAST.gitlab-ci.yml) to reflect the new scanning coverage and to remove language-specific analyzer jobs that are no longer used.
