## Background

General Handbook Entry https://about.gitlab.com/handbook/engineering/development/processes/Infra-Dev-Escalation/
Process https://handbook.gitlab.com/handbook/engineering/development/processes/infra-dev-escalation/process/
See epic https://gitlab.com/groups/gitlab-com/-/epics/122


## Action items and due dates

### Sign up - ~1st-15th of the previous month

* [ ] Coordinator to update the [sign up sheet's](https://docs.google.com/spreadsheets/d/10uI2GzqSvITdxC5djBo3RN34p8zFfxNASVnFlSh8faU/edit?gid=1680755833#gid=1680755833) **Coordinator** column. 

### Reminders - ~15th of the previous month

* [ ] Coordinator calling for sign-up and doing assignments:
  * [ ] Posting reminders to the #development and #backend channels in Slack
  * [ ] Ask managers in #eng-managers to remind team-members in 1-1s

 ### Manual assignment - ~25th of the previous month 

* Find available engineers quickly without going through multiple spreadsheets using the [Dev-on-call](https://gitlab.com/gitlab-com/dev-on-call) tool. See also the video on how it works (use [GitLab unfiltered](https://handbook.gitlab.com/handbook/marketing/marketing-operations/youtube/#unable-to-view-a-video-on-youtube)): https://www.youtube.com/watch?v=D0bR0WnvdgM
* Alternatively, the manual process for coordinators is documented in this process page [how-to](https://handbook.gitlab.com/handbook/engineering/development/processes/infra-dev-escalation/process/#how-to)

_Note the recommendation to check the team members' calendar to ensure they are available. This will help ensure team members are not scheduled when on leave, and reduce the need for rescheduling._

* [ ] Coordinator nomination: all **assigned** engineers are notified by mentioning them by name in this issue **explicitly**
  * [ ] Engineers who are on leave should inform Coordinator and the Coordinator should find an appropriate engineer.  
  * [ ] Engineers are encouraged to help the Coordinator by seeking replacements for time-slots that don't work for them.  Swap for like days/shifts later in the schedule by reaching out to your peers directly. Engineers who are unsuccessful in finding a replacement please work with Coordinator to find a solution. 

### Calendar sync - ~2 days before the 1st of the month

* [ ] All slots are signed up
* [ ] [DRI](https://handbook.gitlab.com/handbook/engineering/development/processes/infra-dev-escalation/process/#development-on-call-dri) (@jameslopez) publishes on-call schedule to the [shared calendar](https://calendar.google.com/calendar/u/0/embed?src=gitlab.com_vj98gounb5e3jqmkmuvdu5p7k8@group.calendar.google.com) when all slots are confirmed. (see instructions above)

### After the scheduled month

* Coordinator can close this issue

### Examples

**Example Slack Messages**

Example text for on-call message, to be posted in `#development` and `#backend`:

<details>
<summary>Click to expand</summary>

```
:speaker: Hi everyone! I am putting together the MONTH dev on-call schedule. Thank you if you have already signed up!  If you haven't, please consider adding your names now in order to select your preferred time slots.
In the upcoming week I will start assigning the remaining slots based on previous participation :thank_you:
```
- Add link to the on-call spreadsheet

Example text to be posted in `#eng-managers`
```
Engineering Managers I am putting together the MONTH dev on-call schedule. Please mention it to your team members in upcoming 1:1s.
```
- Add link to the on-call spreadsheet
- Add link to message posted in #development

</details>

**Example Assignment confirmation**
<details>
<summary>Click to expand</summary>

```
Hey Engineering Crew ✨ 😊 ✨

I am your humble coordinator for our [CHANGE to month/year] Infra/Dev Escalation On-Call schedule.
You are being copied here because I have assigned you a rotation slot for [CHANGE to month/year] 

#### Instructions

Review the [CHANGE to month/year]  (aka [CHANGE to yyyy-mm]) tab on the Infra/Dev Escalation On-Call Schedule for your assignment [here](https://docs.google.com/spreadsheets/d/10uI2GzqSvITdxC5djBo3RN34p8zFfxNASVnFlSh8faU/edit?gid=1729753107#gid=1729753107)

If you cannot fill the slot for which you are assigned, please do your best to find someone with the same time and day of the week obligation on a different day of the month to swap using the #development Slack channel (e.g. swap Saturday, September 14 from 0400-0759 for Saturday, September 28 0400-0759)
Our first and best option is to swap people within the same month. If you cannot find someone to swap days with, please comment on the slot in the spreadsheet and we will do our best to find another person to swap.

#### Handbook Reference
There are many first time on-call participants here. If you have any questions about why we do on-call support please read the handbook entry [here](https://handbook.gitlab.com/handbook/engineering/development/processes/infra-dev-escalation/) and the [eligibility and guidelines section](https://handbook.gitlab.com/handbook/engineering/development/processes/infra-dev-escalation/process/#guidelines).

**Why was I selected for on call ? .. A variety of criteria were used to find folks who were available.** 

* Time zone availability
* Number of on-call shifts taken in the past (see https://gitlab.com/gitlab-com/dev-on-call)
* Number of on-call shifts taken this quarter

#### Crew Members Selected

[CHANGE to list of direct pings to engineers]
```

</details>
/label ~"Engineering Management" 
/epic https://gitlab.com/groups/gitlab-com/-/epics/122
